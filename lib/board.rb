class Board
  attr_accessor :grid
  def initialize(grid = nil)
    if grid.nil?
      @grid = [
        [nil, nil, nil],
        [nil, nil, nil],
        [nil, nil, nil]
      ]
    else
      @grid = grid
    end
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark
  end

  def place_marks(pos1, pos2, pos3, mark)
    grid[pos1[0]][pos1[1]] = mark
    grid[pos2[0]][pos2[1]] = mark
    grid[pos3[0]][pos3[1]] = mark
  end

  def three_in_a_row(pos1, pos2, pos3)
    pos1 == pos2 && pos2 == pos3 && !pos1.nil?
  end

  def winner
    # rows
    if three_in_a_row(grid[0][0], grid[0][1], grid[0][2])
      return grid[0][0]
    elsif three_in_a_row(grid[1][0], grid[1][1], grid[1][2])
      return grid[1][0]
    elsif three_in_a_row(grid[2][0], grid[2][1], grid[2][2])
      return grid[2][0]
    # diagonals
    elsif three_in_a_row(grid[0][0], grid[1][1], grid[2][2])
      return grid[0][0]
    elsif three_in_a_row(grid[0][2], grid[1][1], grid[2][0])
      return grid[0][2]
    # columns
    elsif three_in_a_row(grid[0][0], grid[1][0], grid[2][0])
      return grid[0][0]
    elsif three_in_a_row(grid[0][1], grid[1][1], grid[2][1])
      return grid[0][1]
    elsif three_in_a_row(grid[0][2], grid[1][2], grid[2][2])
      return grid[0][2]
    else
      nil
    end
  end

  def empty?(pos)
    grid[pos[0]][pos[1]].nil?
  end

  def over?
    if grid.flatten.all? { |empty| empty == nil }
      false
    elsif !winner.nil?
      true
    elsif grid.flatten.include?(nil)
      false
    elsif !grid.flatten.include?(nil)
      true
    end
  end

end
