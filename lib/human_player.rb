class HumanPlayer
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where do you want to move?"
    human_move = gets.chomp
    [human_move[0].to_i, human_move[-1].to_i]
  end

  def display(board)
    puts board.grid
  end
end
