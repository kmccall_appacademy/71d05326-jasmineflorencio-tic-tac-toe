class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    nil_spots = []
    board.grid.each_with_index do |rows, idx|
      rows.each_with_index do |column, idx2|
        if board.grid[idx][idx2].nil?
          nil_spots << [idx,idx2]
          board.grid[idx][idx2] = mark
          if !board.winner.nil?
            return [idx, idx2]
          end
          board.grid[idx][idx2] = nil
        end
      end
    end
    nil_spots.sample
  end

end
