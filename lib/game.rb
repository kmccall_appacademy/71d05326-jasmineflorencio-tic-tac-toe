require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two
  attr_accessor :board
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
  end

  def play_turn
    move = @player_one.get_move
    @board.place_mark(move, @player_one.mark)
    self.switch_players!
  end

  def switch_players!
    @player_one, @player_two = @player_two, @player_one
  end

  def current_player
    @player_one
  end

end
